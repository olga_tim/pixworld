const gulp = require('gulp');

//server

const server = require('gulp-connect-multi')();

gulp.task('server', server.server({
	host: '127.0.0.1',
	port: 9090,
	root: ['site'],
	livereload: true
}));

//templates

const htmlmin = require('gulp-htmlmin');
gulp.task('templates', () => {
  return gulp.src('./dev/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./site'))
    .pipe(server.reload());
});

//styles

const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const csso = require('gulp-csso');
gulp.task('styles', () => {
  return gulp.src('./dev/scss/style.scss')
	  .pipe(sass())
	  .pipe(autoprefixer({browsers: ['last 2 versions']}))
	  .pipe(csso())
    .pipe(gulp.dest('./dev/css'))
	  .pipe(gulp.dest('./site/css'))
    .pipe(server.reload());
});

//scripts

const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

gulp.task('scripts', () => {
  return gulp.src('./dev/js/*.js')
    .pipe(concat('script.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./site/js'))
    .pipe(server.reload());
});

//images

const imagemin = require('gulp-imagemin');
gulp.task('images', () => {
    return gulp.src('./dev/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('site/images'))
});

//watcher

gulp.task('watch', () => {
	gulp.watch('./dev/*.html', ['templates']);
	gulp.watch('./dev/scss/**/*.scss', ['styles']);
	gulp.watch('./dev/js/*.js', ['scripts']);
	gulp.watch('./images/*.{png,jpg,jpeg,gif,svg}', {cwd:'./dev/'}, ['images']);
});



//default

gulp.task('default', ['templates', 'styles', 'scripts', 'images']);
gulp.task('dev', ['default', 'server', 'watch']);